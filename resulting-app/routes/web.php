<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ExportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/export/player-stats', [ExportController::class, 'stats']);
Route::get('/export/player', [ExportController::class, 'index']);


// /php-tests/export.php?type=playerstats&team=TOR
// /php-tests/export.php?type=playerstats&position=C
// /php-tests/export.php?type=playerstats&player=Steven%20Adams&format=xml
// /php-tests/export.php?type=players&player=Steven%20Adams&format=json
// /php-tests/export.php?type=players&team=HOU&format=xml
