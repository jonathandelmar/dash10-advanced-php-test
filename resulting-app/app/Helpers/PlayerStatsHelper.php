<?php

namespace App\Helpers;

class PlayerStatsHelper
{
    /**
     * Returns the total point given the player stats
     *
     * @param integer $threePt
     * @param integer $twoPt
     * @param integer $freeThrows
     * @return integer
     */
    public function getTotalPoints(int $threePt = 0, int $twoPt = 0, int $freeThrows = 0): int
    {
        return ($threePt * 3) + ($twoPt * 2) + $freeThrows;
    }

    /**
     * Computes the shot percentage
     *
     * @param integer $counted
     * @param integer $attempted
     * @return integer
     */
    public function shotPercentage(int $counted = 0, int $attempted = 0): int
    {
        return $attempted ? (round($counted / $attempted, 2) * 100) : 0;
    }

    /**
     * Computes the total number of rebounds
     *
     * @param integer $offensive
     * @param integer $defensive
     * @return integer
     */
    public function totalRebounds(int $offensive, int $defensive): int
    {
        return $offensive + $defensive;
    }


}
