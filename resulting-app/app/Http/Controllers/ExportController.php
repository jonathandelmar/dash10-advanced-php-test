<?php

namespace App\Http\Controllers;

use App\Services\{
    PlayerService,
    PlayerStatsService
};
use Illuminate\Http\Request;

class ExportController extends Controller
{
    public function __construct(Request $request)
    {
        $allowedSearchArgs = ['player', 'playerId', 'team', 'position', 'country'];
        $this->filters = collect($request->all())->filter(function ($value, $key) use ($allowedSearchArgs) {
            return in_array($key, $allowedSearchArgs);
        });
    }

    public function index(Request $request, PlayerService $service)
    {
        $data = $service->getByFilters($this->filters);
        $data = $data->map(function ($stat) {
            $stat = collect($stat);
            $stat->pull('id');
            return $stat;
        });

        if ($request->input('format') == 'json') {
            return response()->json($data);
        }

        return view('export.player', [
            'data' => $data
        ]);
    }

    public function stats(Request $request, PlayerStatsService $service)
    {
        $data = $service->getByFilters($this->filters);
        $data = $service->populateStatsTotals($data);
        $data = $data->map(function ($stat) {
            $stat->pull('id');
            $stat->pull('player_id');
            return $stat;
        });

        if ($request->input('format') == 'json') {
            return response()->json($data);
        }

        return view('export.player-stats', [
            'data' => $data
        ]);
    }
}
