<?php
namespace App\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class PlayerQueryFilterService
{
    /**
     * Apply filters to the given query when possible
     *
     * @param Builder $query
     * @param Collection $filters
     * @return Builder
     */
    public function applyQueryFilters(Builder $query, Collection $filters): Builder
    {
        if ($filters->has('playerId')) {
            $query = $query->whereId($filters['playerId']);
        }
        if ($filters->has('player')) {
            $query = $query->whereName($filters['player']);
        }
        if ($filters->has('team')) {
            $query = $query->whereTeamCode($filters['team']);
        }
        if ($filters->has('position')) {
            $query = $query->wherePosition($filters['position']);
        }
        if ($filters->has('country')) {
            $query = $query->whereNationality($filters['country']);
        }

        return $query;
    }
}
