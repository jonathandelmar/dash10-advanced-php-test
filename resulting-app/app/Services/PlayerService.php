<?php

namespace App\Services;

use App\Models\{
    Player,
    Stats
};
use Illuminate\Support\Collection;

class PlayerService
{
    /**
     * Returns the collection statistics of a player
     *
     * @param Collection $filters
     * @return Collection
     */
    public function getByFilters(Collection $filters): Collection
    {
        $query = Player::whereNotNull('id'); // noop alternative
        return (new PlayerQueryFilterService)->applyQueryFilters($query, $filters)->get();
    }
}
