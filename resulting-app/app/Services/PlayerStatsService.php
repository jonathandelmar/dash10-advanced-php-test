<?php

namespace App\Services;

use App\Helpers\PlayerStatsHelper;
use App\Models\{
    Player,
    Stats
};
use Illuminate\Support\Collection;

class PlayerStatsService
{
    /**
     * Returns the collection statistics of a player
     *
     * @param Collection $filters
     * @return Collection
     */
    public function getByFilters(Collection $filters): Collection
    {
        $statsDbTable = with(new Stats)->getTable();
        $playerDbTable = with(new Player)->getTable();

        $query = Player::join(
            $statsDbTable,
            "${playerDbTable}.id",
            '=',
            "${statsDbTable}.player_id"
        );

        return (new PlayerQueryFilterService)->applyQueryFilters($query, $filters)->get();
    }

    /**
     * Add more total stats to a given collection data
     *
     * @param Collection $data
     * @return Collection
     */
    public function populateStatsTotals(Collection $data): Collection
    {
        $helper = new PlayerStatsHelper;
        return $data->map(function ($stat) use ($helper) {
            $statArr = $stat->toArray();
            $statArr['total_points'] = $helper->getTotalPoints($statArr['3pt'], $statArr['2pt'], $statArr['free_throws']);
            $statArr['field_goals_pct'] = $helper->shotPercentage($statArr['field_goals'], $statArr['field_goals_attempted']);
            $statArr['3pt_pct'] = $helper->shotPercentage($statArr['3pt'], $statArr['3pt_attempted']);
            $statArr['2pt_pct'] = $helper->shotPercentage($statArr['2pt'], $statArr['2pt_attempted']);
            $statArr['free_throws_pct'] = $helper->shotPercentage($statArr['free_throws'], $statArr['free_throws_attempted']);
            $statArr['total_rebounds'] = $helper->totalRebounds($statArr['offensive_rebounds'], $statArr['defensive_rebounds']);
            return collect($statArr);
        });
    }
}
