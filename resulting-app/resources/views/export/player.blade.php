@extends('layouts.master')

@section('content')

<table class="table table-striped table-bordered table-hover">
    <thead class="thead-light">
        @foreach ($data as $id => $item)
        <tr>
            @foreach($item as $title => $value)
            <th scope="col">{{ Str::of($title)->replace('_', ' ')->title() }}</th>
            @endforeach
        </tr>
        @break
        @endforeach
    </thead>
    <tbody>
        @foreach ($data as $id => $item)
        <tr>
            @foreach($item as $title => $value)
            <td>{{ $value }}</td>
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>

@stop